import numpy as np 
from astropy.cosmology import Planck18
from scipy.optimize import brentq
from astropy.units import km, s, Mpc, yr, Msun, Gyr, pc, kpc
import astropy.constants as const

# Here we use the following units:
# Mass: Solar masses
# Distance: Mpc
# Time: yr
# Velocity: Mpc/yr

# define NFW profile
def nfw(r, rho0, rs):
    ''' NFW profile 
      
    Parameters
    r: radius
    rho0: central density
    rs: scale radius
    
    Returns
    rho: density
    '''
    return rho0 / (r/rs * (1+r/rs)**2)

# define NFW profile surface mass density
def nfw_smd(R, rho0, rs):
    ''' NFW profile surface mass density
        
    Parameters
    R: radial distance perpendicular to the line of sight
    rho0: central density
    rs: scale radius
    
    Returns
    Sigma: surface mass density
    '''
    x = (R/rs).to_value("1")
    if x<1:
        Sigma = 2*rs*rho0/(x**2-1)* (1-2/np.sqrt(1-x**2)*np.arctanh(np.sqrt((1-x)/(1+x))))
    elif x==1:
        Sigma = 2*rs*rho0/3
    else:
        Sigma = 2*rs*rho0/(x**2-1)* (1-2/np.sqrt(x**2-1)*np.arctan(np.sqrt((x-1)/(1+x))))
    return Sigma

# define concentration mass relationships for the NFW profile
def get_concentration_mass_relation(mvir, zl, model='Hannuksela2020'):
    ''' Concentration-mass relation for the NFW profile

    Parameters
    mvir: virial halo mass
    zl: redshift
    model: concentration-mass relation model

    Returns
    c: concentration
    '''
    if model=='diemer19':
        c = 5.71 * (mvir/1e12)**(-0.084) * (1+zl)**(-0.47)
    elif model=='Duffy08':
        c = 10**(0.905-0.101*(1+zl))*((mvir/1e12)**(-0.101))
    elif model=='Hannuksela2020':
        h0,c0,c1,c2,c3,c4,c5 = 0.67,37.5153,-1.5093,1.636e-2,3.66e-4,-2.89237e-5,5.32e-7
        param = h0*mvir/Msun
        c = c5*np.log(param)**5 + c4*np.log(param)**4 + c3*np.log(param)**3 + c2*np.log(param)**2 + c1*np.log(param) + c0 
    else:
        raise ValueError('model not supported')
    return c

def get_nfw_parameters_from_mass_concentration(mvir, c, zl):
    ''' Compute NFW parameters from halo mass and concentration

    Parameters
    mvir: virial halo mass
    c: concentration
    zl: redshift

    Returns
    rho0: central density
    rs: scale radius
    '''
    delta_c = 200./3. * c**3 / (np.log(1+c) - c/(1+c))
    # Compute rho_c with solar mass/Mpc^3 units
    H = Planck18.H(zl)
    rho_c = 3*H**2/(8*np.pi*const.G)
    rho0 = delta_c*rho_c
    r200 = (3*mvir/(4*np.pi*200*rho_c))**(1/3)
    rs = r200/c
    return rho0, rs

# Define the virial radius
def get_virial_radius_from_virial_mass(mvir, zl):
    ''' Virial theorem

    Parameters
    mvir: virial halo mass
    zl: redshift

    Returns
    rvir: virial radius
    '''
    H = Planck18.H(zl)
    rvir = (mvir*const.G/(100*H**2))**(1/3) # Mpc
    return rvir

# define the velocity dispersion virial mass relationship
def get_mvir_from_velocity_dispersion(sigma_v, zl):
    ''' Velocity dispersion-mass relation

    Parameters
    sigma_v: velocity dispersion
    zl: redshift
    model: velocity dispersion-mass relation model

    Returns
    mvir: virial halo mass
    '''
    rvir = lambda mvir: get_virial_radius_from_virial_mass(mvir, zl)
    def eq(mvir):
        mvir = mvir*Msun
        return (sigma_v**2- const.G*mvir/rvir(mvir)).to_value("km^2/s^2")
    #eq = lambda mvir: sigma_v**2 - G*mvir/rvir(mvir)
    mvir = brentq(eq, 100, 1e17)*Msun
    return mvir

# Get the NFW profile parameters using nothing but the velocity dispersion
def get_nfw_parameters_from_velocity_dispersion(sigma_v, zl):
    ''' Compute NFW parameters from velocity dispersion

    Parameters
    sigma_v: velocity dispersion
    zl: redshift

    Returns
    rho0: central density
    rs: scale radius

    -------
    Example
    -------
    from astropy.cosmology import Planck18
    from astropy.units import km, s, Mpc, yr, Msun, Gyr, pc, kpc
    import astropy.constants as const
    sigma_v = 200*km/s
    zl=0.5
    rho0, rs = get_nfw_parameters_from_velocity_dispersion(sigma_v, zl)
    print("rho0 [Msun/Mpc^3]", rho0.to("Msun/Mpc^3"))
    print("rs [Mpc]", rs.to("Mpc"))
    # Compute the surface mass density at Einstein radius
    zs = 2
    Dls= Planck18.angular_diameter_distance_z1z2(zl, zs)
    Ds = Planck18.angular_diameter_distance(zs)
    Dl = Planck18.angular_diameter_distance(zl)
    theta_E = 4*np.pi*(sigma_v/const.c)**2 * Dls/Ds
    r_E = theta_E*Dl
    Sigma_at_Einstein_radius = nfw_smd(r_E, rho0, rs) # Sigma(r_E)
    print("Sigma [Msun/pc^2]", Sigma_at_Einstein_radius.to("Msun/pc^2"))
    '''
    mvir = get_mvir_from_velocity_dispersion(sigma_v, zl)
    c = get_concentration_mass_relation(mvir, zl)
    rho0, rs = get_nfw_parameters_from_mass_concentration(mvir, c, zl)
    return rho0, rs



def get_Sigma_at_Einstein_radius(sigma_v, zl, zs):
    ''' Compute the surface mass density at Einstein radius

    Parameters
    sigma_v: velocity dispersion
    zl: redshift of the lens
    zs: redshift of the source

    Returns
    Sigma: surface mass density at Einstein radius

    -------
    Example
    -------
    from astropy.cosmology import Planck18
    from astropy.units import km, s, Mpc, yr, Msun, Gyr, pc, kpc
    import nfw
    sigma_v = 200*km/s
    zl=0.5
    zs=2
    Sigma = nfw.get_Sigma_at_Einstein_radius(sigma_v, zl, zs)
    print("Sigma [Msun/pc^2]", Sigma.to("Msun/pc^2"))
    '''
    rho0, rs = get_nfw_parameters_from_velocity_dispersion(sigma_v, zl)
    Dls= Planck18.angular_diameter_distance_z1z2(zl, zs)
    Ds = Planck18.angular_diameter_distance(zs)
    Dl = Planck18.angular_diameter_distance(zl)
    theta_E = 4*np.pi*(sigma_v/const.c)**2 * Dls/Ds
    r_E = theta_E*Dl
    Sigma = nfw_smd(r_E, rho0, rs) # Sigma(r_E)
    return Sigma

def get_diego_optical_depth_from_vel_dispersion(sigma_v, zl, zs, mu_r, mu_t, lens_model='point_mass_lens'):
    ''' Compute the effective optical depth at Einstein radius (note that this is different from the strong lensing optical depth of the galaxy)

    Parameters
    sigma_v: velocity dispersion
    zl: redshift of the lens
    zs: redshift of the source
    mu_r: radial magnification
    mu_t: tangential magnification

    Returns
    tau_eff: effective optical depth at Einstein radius (note that this is different from the strong lensing optical depth of the galaxy)

    ---------
    Example 1
    ---------
    from astropy.cosmology import Planck18
    from astropy.units import km, s, Mpc, yr, Msun, Gyr, pc, kpc
    sigma_v = 200*km/s
    zl=0.5
    zs=2
    tau_eff = get_diego_optical_depth_from_vel_dispersion(sigma_v, zl, zs, mu_r=2, mu_t=5)
    print("tau_eff", tau_eff)

    ---------
    Example 2
    ---------
    from astropy.cosmology import Planck18
    from astropy.units import km, s, Mpc, yr, Msun, Gyr, pc, kpc
    import pylab as plt
    import numpy as np
    N = 100
    sigma_v = np.linspace(100, 500, N)*km/s
    zl=0.5
    zs=2
    tau_eff = np.zeros(N)
    for i in range(N):
        tau_eff[i] = get_diego_optical_depth_from_vel_dispersion(sigma_v[i], zl, zs, mu_r=2, mu_t=5)
    plt.plot(sigma_v, tau_eff)
    plt.xlabel("sigma_v [km/s]")
    plt.ylabel("tau_eff")
    plt.show()
    '''
    Sigma = get_Sigma_at_Einstein_radius(sigma_v, zl, zs)
    if lens_model == 'point_mass_lens':
        tau_eff = 4.2e-4*Sigma.to_value("Msun/pc^2")*mu_t
    else:
        raise ValueError("lens_model not implemented")
    return tau_eff

def get_diego_optical_depth_from_mvir(mvir, zl, zs, mu_r, mu_t, lens_model='point_mass_lens'):
    ''' Compute the effective optical depth at Einstein radius (note that this is different from the strong lensing optical depth of the galaxy)

    Parameters
    mvir: virial mass
    zl: redshift of the lens
    zs: redshift of the source
    mu_r: radial magnification
    mu_t: tangential magnification

    Returns
    tau_eff: effective optical depth at Einstein radius (note that this is different from the strong lensing optical depth of the galaxy)

    ---------
    Example 1
    ---------
    from astropy.cosmology import Planck18
    from astropy.units import km, s, Mpc, yr, Msun, Gyr, pc, kpc
    mvir = 1e14*Msun
    zl=0.5
    zs=2
    tau_eff = get_diego_optical_depth_from_mvir(mvir, zl, zs, mu_r=2, mu_t=5)
    print("tau_eff", tau_eff)

    ---------
    Example 2
    ---------
    from astropy.cosmology import Planck18
    from astropy.units import km, s, Mpc, yr, Msun, Gyr, pc, kpc
    import pylab as plt
    import numpy as np
    N = 100
    mvir = np.linspace(1e13, 1e15, N)*Msun
    zl=0.5
    zs=2
    tau_eff = np.zeros(N)
    for i in range(N):
        tau_eff[i] = get_diego_optical_depth_from_mvir(mvir[i], zl, zs, mu_r=2, mu_t=5)
    plt.plot(mvir, tau_eff)
    plt.xlabel("mvir [Msun]")
    plt.ylabel("tau_eff")
    plt.show()
    '''
    c = get_concentration_mass_relation(mvir, zl)
    rho0, rs = get_nfw_parameters_from_mass_concentration(mvir, c, zl)
    Dls= Planck18.angular_diameter_distance_z1z2(zl, zs)
    Ds = Planck18.angular_diameter_distance(zs)
    Dl = Planck18.angular_diameter_distance(zl)
    rvir = get_virial_radius_from_virial_mass(mvir, zl)
    sigma_v = np.sqrt( const.G*mvir/rvir ) # virial velocity dispersion
    theta_E = 4*np.pi*(sigma_v/const.c)**2 * Dls/Ds
    r_E = theta_E*Dl
    Sigma = nfw_smd(r_E, rho0, rs) # Sigma(r_E)
    if lens_model == 'point_mass_lens':
        tau_eff = 4.2e-4*Sigma.to_value("Msun/pc^2")*mu_t
    else:
        raise ValueError("lens_model not implemented")
    return tau_eff














