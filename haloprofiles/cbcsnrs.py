import numpy as np
import bilby
from bilby.core import utils
from pycbc.detector import Detector

class CBCSNRsBase():
    def __init__(self, mass_1, mass_2, luminosity_distance, a_1, a_2, tilt_1, tilt_2, phi_12, phi_jl, ra, dec, psi, theta_jn, phase, geocent_time, sampling_frequency=4096, waveform_arguments=  dict(waveform_approximant = "IMRPhenomXPHM", reference_frequency = 50., minimum_frequency = 40), list_of_detectors=["H1", "L1", "V1"]):
        nsamples = len(mass_1) # Set to be the same size as the input vector
        # Initialize PSDs and IFOs
        self.__initialize_psds()
        self.__initialize_ifos(list_of_detectors)
        self.list_of_detectors = list_of_detectors
        self.waveform_arguments = waveform_arguments
        self.sampling_frequency = sampling_frequency
        # Save the inner products
        hp_inner_hp = dict()
        hp_inner_hc = dict()
        hc_inner_hc = dict()
        for ifo in list_of_detectors:
            hp_inner_hp[ifo] = []
            hp_inner_hc[ifo] = []
            hc_inner_hc[ifo] = []
            for i in range(nsamples):
                hp_inner_hp_i, hp_inner_hc_i, hc_inner_hc_i = self.inner_products(i, mass_1, mass_2, a_1, a_2, tilt_1, tilt_2, phi_12, phi_jl, luminosity_distance, ra, dec, psi, theta_jn, phase, geocent_time)
                hp_inner_hp[ifo].append(hp_inner_hp_i[ifo])
                hp_inner_hc[ifo].append(hp_inner_hc_i[ifo])
                hc_inner_hc[ifo].append(hc_inner_hc_i[ifo])
            hp_inner_hp[ifo] = np.array(hp_inner_hp[ifo]) 
            hp_inner_hc[ifo] = np.array(hp_inner_hc[ifo]) 
            hc_inner_hc[ifo] = np.array(hc_inner_hc[ifo]) 
        self.hp_inner_hp = hp_inner_hp
        self.hp_inner_hc = hp_inner_hc
        self.hc_inner_hc = hc_inner_hc
        base_samples = dict()
        for variable in ('mass_1', 'mass_2', 'luminosity_distance', 'a_1', 'a_2', 'tilt_1', 'tilt_2', 'phi_12', 'phi_jl', 'ra', 'dec', 'psi', 'theta_jn', 'phase', 'geocent_time', 'hp_inner_hp', 'hp_inner_hc', 'hc_inner_hc'):
            base_samples[variable] = locals()[variable]
        self.base_samples = base_samples
        self.nsamples = nsamples
        return

    def inner_product_simple(self, parameters):
        # now we can setup the waveform generator to 
        # make the two polarizations
        # first need to have an approximate signal duration
        approx_duration = bilby.gw.utils.calculate_time_to_merger(self.waveform_arguments['minimum_frequency'],
                                                                  parameters['mass_1'],
                                                                  parameters['mass_2'],
                                                                  safety = 1.2)
        duration = np.ceil(approx_duration + 4.)
        utils.logger.disabled = True # Stop debug messages
        waveform_generator = bilby.gw.WaveformGenerator(duration = duration, sampling_frequency = self.sampling_frequency, frequency_domain_source_model = bilby.gw.source.lal_binary_black_hole, waveform_arguments = self.waveform_arguments)
        utils.logger.disabled = False # Enable debug messages (this allows us to avoid spam)
        polas = waveform_generator.frequency_domain_strain(parameters = parameters)
        done_ifos = []
        done_psds = []
        hp_inner_hp = dict()
        hp_inner_hc = dict()
        hc_inner_hc = dict()
        for ifo in self.list_of_detectors:
            if self.psds[ifo] in done_psds:
                # already computed the inner product for 
                # the corresponsing PSD
                seen_ifo = done_ifos[done_psds.index(self.psds[ifo])]
                hp_inner_hp[ifo] = hp_inner_hp[seen_ifo]
                hp_inner_hc[ifo] = hp_inner_hc[seen_ifo]
                hc_inner_hc[ifo] = hc_inner_hc[seen_ifo]
            else:
                # need to compute the inner product for
                # the firs time
                p_array = self.psds_arrays[ifo].get_power_spectral_density_array(waveform_generator.frequency_array)
                hp_inner_hp[ifo] = bilby.gw.utils.noise_weighted_inner_product(polas['plus'],
                                                                               polas['plus'],
                                                                               p_array,
                                                                               waveform_generator.duration)
                hp_inner_hc[ifo] = bilby.gw.utils.noise_weighted_inner_product(polas['plus'],
                                                                               polas['cross'],
                                                                               p_array,
                                                                               waveform_generator.duration)
                hc_inner_hc[ifo] = bilby.gw.utils.noise_weighted_inner_product(polas['cross'],
                                                                               polas['cross'],
                                                                               p_array,
                                                                               waveform_generator.duration)
                done_ifos.append(ifo)
                done_psds.append(self.psds[ifo])
        return hp_inner_hp, hp_inner_hc, hc_inner_hc


    def inner_products(self, i, mass_1, mass_2, a_1, a_2, tilt_1, tilt_2, phi_12, phi_jl, luminosity_distance, ra, dec, psi, theta_jn, phase, geocent_time):
        parameters = dict(mass_1 = mass_1[i], mass_2 = mass_2[i],
                          a_1 = a_1[i], a_2 = a_2[i],
                          tilt_1 = tilt_1[i], tilt_2 = tilt_2[i],
                          phi_12 = phi_12[i], phi_jl = phi_jl[i],
                          luminosity_distance = luminosity_distance[i],
                          ra = ra[i],
                          dec = dec[i],
                          psi = psi[i],
                          theta_jn = theta_jn[i],
                          phase = phase[i],
                          geocent_time = geocent_time[i])
        hp_inner_hp, hp_inner_hc, hc_inner_hc = self.inner_product_simple(parameters)
        return hp_inner_hp, hp_inner_hc, hc_inner_hc
    
    def __initialize_psds(self):
        psds = dict()
        psds['L1'] = 'aLIGO_O4_high_asd.txt'
        psds['H1'] = 'aLIGO_O4_high_asd.txt'
        psds['V1'] = 'AdV_asd.txt'
        psds['K1'] = 'KAGRA_design_asd.txt'
        psds['a_1'] = 'aLIGO_O4_high_asd.txt'
        psd_file=False
        # load psds once and for all as useful objects
        psds_arrays = dict()
        if psd_file:
            for key in psds:
                psds_arrays[key] = bilby.gw.detector.PowerSpectralDensity(psd_file = psds[key])
        else:
            for key in psds:
                psds_arrays[key] = bilby.gw.detector.PowerSpectralDensity(asd_file = psds[key])
        self.psds = psds
        self.psds_arrays = psds_arrays
        return psds, psds_arrays
    
    def __initialize_ifos(self, list_of_detectors):
        # make the ifos as bilby ifo's object to be used later
        ifos_objects = dict()
        for key in list_of_detectors:
            ifos_objects[key] = bilby.gw.detector.networks.get_empty_interferometer(key)
        self.ifos_objects = ifos_objects
        # Create fast ifo
        fast_ifo_objects = dict()
        for detector in list_of_detectors:
            fast_ifo_object = Detector(detector) # create pycbc detector
            fast_ifo_objects[detector] = fast_ifo_object
        self.fast_ifo_objects = fast_ifo_objects
        return ifos_objects, fast_ifo_objects
   
class CBCSNRsLensed(CBCSNRsBase):
    def __init__(self, mass_1, mass_2, luminosity_distance, a_1, a_2, tilt_1, tilt_2, phi_12, phi_jl, ra, dec, psi, theta_jn, phase, geocent_time, sampling_frequency=4096, waveform_arguments=  dict(waveform_approximant = "IMRPhenomXPHM", reference_frequency = 50., minimum_frequency = 40), list_of_detectors=["H1", "L1", "V1"] ):
        super(CBCSNRsLensed, self).__init__(  mass_1, mass_2, luminosity_distance, a_1, a_2, tilt_1, tilt_2, phi_12, phi_jl, ra, dec, psi, theta_jn, phase, geocent_time, sampling_frequency, waveform_arguments, list_of_detectors )
        self.nsamples_intrinsic = self.nsamples
        self.intrinsic_variable_names = 'mass_1', 'mass_2', 'luminosity_distance', 'a_1', 'a_2', 'tilt_1', 'tilt_2', 'phi_12', 'phi_jl','ra', 'dec', 'psi', 'theta_jn', 'phase', 'geocent_time', 'hp_inner_hp', 'hp_inner_hc', 'hc_inner_hc'
        # Per-image variables
        self.image_specific_names = 'td','mu','net_snr','snrs_sq', 'luminosity_distance_effective', 'geocent_time_effective'

    def draw_binaries( self, td, mu, n_images = 2 ):
        ''' Draws randomly binaries from the base unlensed binary samples and creates a lensed population from them. Inputs are the lensed arrival times td and magnifications mu (all dictionaries containing x[img][values]). We assume negligible effect of the morse phase on the SNR. n_images=2 is the number of images.
        '''
        if 'img0' not in td or 'img0' not in mu:
            raise ValueError("Bad input for magnification/time delay: Must contain mu['imgX'][values] where imgX is the image id, typically img0, img1, img2, ....")
        # Draw a number of intrinsic samples
        sample_intrinsic = self.__draw_intrinsic_sample(  )
        # Draw a number of  image properties
        sample_image_properties = self.__draw_image_property_sample( td, mu, n_images)
        # Combine dictionaries
        samples = {**sample_intrinsic, **sample_image_properties}
        # Get the SNRs
        samples = self.compute_lensed_snr(samples, n_images)
        return samples

    def __draw_image_property_sample(self, td, mu, n_images):
        parameters = dict()
        for j in range(n_images):
            img = "img%d"%j
            parameters[img] = dict()
            parameters[img]['mu'] = mu[img]
            parameters[img]['td'] = td[img]
        return parameters

    def __draw_intrinsic_sample(self):
        parameters = dict()
        for variable in ('mass_1', 'mass_2', 'luminosity_distance', 'a_1', 'a_2', 'tilt_1', 'tilt_2', 'phi_12', 'phi_jl','ra', 'dec', 'psi', 'theta_jn', 'phase', 'geocent_time'):
            parameters[variable] = np.array(self.base_samples[variable])
        for variable in ('hp_inner_hp', 'hp_inner_hc', 'hc_inner_hc'):
            parameters[variable] = dict()
            for ifo in self.list_of_detectors:
                parameters[variable][ifo] = self.base_samples[variable][ifo]
        return parameters

    def compute_lensed_snr(self, samples, n_images):
        ra, dec, psi = samples['ra'], samples['dec'], samples['psi']
        hp_inner_hp = samples['hp_inner_hp']
        hp_inner_hc = samples['hp_inner_hc']
        hc_inner_hc = samples['hc_inner_hc']
        for j in range(n_images):
            img="img%d"%j
            net_snr_sq = 0
            geocent_time_effective = samples['geocent_time'] + samples[img]['td']
            absmu = np.abs(samples[img]['mu'])
            samples[img]['snrs_sq'] = dict()
            for ifo in self.list_of_detectors:
                # make an ifo object to get the antenna pattern
                Fpl, Fcr = self.fast_ifo_objects[ifo].antenna_pattern(ra, dec, psi, geocent_time_effective)
                snrs_sq = absmu*( (Fpl**2)*hp_inner_hp[ifo] + (Fcr**2)*hc_inner_hc[ifo] + 2*Fpl*Fcr*hp_inner_hc[ifo] )
                net_snr_sq = net_snr_sq + np.real(snrs_sq) # SNR
                samples[img]['snrs_sq'][ifo] = snrs_sq
            net_snr = np.sqrt(net_snr_sq)
            samples[img]['net_snr'] = net_snr
            #samples[img]['snrs_sq'] = snrs_sq.copy()
            samples[img]['geocent_time_effective'] = geocent_time_effective
            samples[img]['luminosity_distance_effective'] = samples['luminosity_distance']/np.sqrt(absmu)
        return samples


