from gwcosmo import priors
import numpy as np

class MassDistribution():
    def __init__(self, **model_pars):
        self.model_pars = model_pars
        self.model_pars_gwcosmo = {'alpha': model_pars['alpha'] ,
                                   'beta': model_pars['beta'],
                                   'delta_m': model_pars['delta_m'],
                                   'mmin': model_pars['mmin'],
                                   'mmax': model_pars['mmax'],
                                   'lambda_peak': model_pars['lam'],
                                   'mu_g': model_pars['mpp'],
                                   'sigma_g': model_pars['sigpp']}
        self.model=priors.mass_prior('BBH-powerlaw-gaussian', self.model_pars_gwcosmo)

    def sample(self, nsamples):
        m01, m02 = self.model.sample(Nsample=nsamples)
        while np.any(m02>m01):
            m01, m02 = self.model.sample(Nsample=nsamples)

        return np.column_stack((m01, m02))

