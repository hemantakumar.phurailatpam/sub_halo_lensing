import numpy as np
from astropy.cosmology import Planck18
from scipy.interpolate import interp1d
from scipy.integrate import quad
from scipy.special import gamma
from astropy import units as u
from scipy.stats import norm
from lenstronomy.LensModel.lens_model import LensModel
from lenstronomy.LensModel.Solver.lens_equation_solver import LensEquationSolver
from lenstronomy.Util.param_util import phi_q2_ellipticity

def RmOguri(zs, R0=23, b2=1.6, b3=2.1, b4=30):
    ''' Oguri 2019 Merger-rate density for Pop-I/II stars

    Parameters
    zs: Source redshift
    R0: Reference rate density
    b2, b3, b4: Fitting parameters (see Oguri 2019)

    Returns
    Rm: BBH merger rate density
    '''
    return R0*(b4+1)*np.exp(b2*zs)/(b4+np.exp(b3*zs)) #Gpc^-3 yr^-1

class UnlensedStatistics():
    def __init__(self, Rm=RmOguri, cosmology=Planck18, zmin=0, zmax=30):
        ''' Initializes the class. Takes as input the merger-rate density and cosmology. 

        Parameters
        Rm: Merger-rate density function
        cosmology: Astropy cosmology object
        zmin, zmax: Minimum and maximum redshifts to consider
        '''
        # Make interpolators
        z_interp_Dc, z_interp_dVcdz, Dc_interp_z, dVcdz_interp_z = make_cosmology_interpolators(cosmology=cosmology)
        self.z_interp_Dc    = z_interp_Dc
        self.z_interp_dVcdz = z_interp_dVcdz
        self.Dc_interp_z    = Dc_interp_z
        self.dVcdz_interp_z = dVcdz_interp_z
        self.Rm             = Rm
        self.zmin           = zmin
        self.zmax           = zmax
        self.rate            = quad(lambda z: self.pzs_unnormed(z), zmin, zmax)[0] # Normalize p(z)
        self.n_images       = 1 # 1 images for the unlensed model
        self.lens_model     = None
        # Uninitalized:
        self.zs_samples     = np.nan
        self.sigma0         = 161
        return

    def rate_of_intrinsic_mergers(self, RmNew):
        ''' Computes the rate of events assuming merger-rate density Rm
            dNl/dt = int R_m(z)/(1+z_s) * dVc/dz * dz  = R int p(z) dz
            Note: If we want to use the old p(z) to compute the rate under a new merger-rate density model, we can use:
            dNl_new/dt = int R_new(z)/(1+z_s) * tau(z_s) * dVc/dz * dz  = R_old int p(z) dz
            dNl_new/dt = int R_old(z)/(1+z_s) * tau(z_s) * dVc/dz * R_new(z)/R_old(z) * dz  = R int R_new(z)/R_old(z) p(z) dz = R_old <R_new(z)/R_old(z)>_p(z)

            Parameters 
            RmNew: New merger-rate density function

            Returns
            rate: Rate of events
        '''
        if self.zs_samples == np.nan:
            zs_samples = self.draw_zs_samples()
        else:
            zs_samples = self.zs_samples
        rate_new = self.rate * np.mean( RmNew(zs_samples)/self.Rm(zs_samples) )
        return rate_new

    def draw_samples(self, nsamples=1000, savesamples = True, seed=None):
        ''' Draw samples from the distribution of intrinsic parameters

        Parameters
        nsamples: Number of samples to draw
        savesamples: If True, saves the samples in the class
        seed: Random seed

        Returns
        samples: Dictionary of samples
        '''
        if seed is not None:
            np.random.seed(seed)
        # Draw source redshifts
        zs_samples = self.draw_zs_samples(nsamples=nsamples)
        # Transform to angular diameter distances (takes a while)
        DS_samples  = Planck18.angular_diameter_distance(zs_samples)
        # Compute weights for each sample:
        sample_weights = np.ones(nsamples)/float(nsamples)
        # Save samples
        samples    = dict(zs=zs_samples,
                          DS=DS_samples,
                          weights=sample_weights)
        if savesamples == True:
            self.samples = samples # All samples here
            self.zs_samples=zs_samples
            self.samples_weights=sample_weights
        return samples

    def pzs(self, zs):
        ''' The redshift prior p(z) is defined with: 
            dNl/dt = int R_m(z)/(1+z_s) * dVc/dz * dz  = R int p(z) dz

            Parameters
            zs: Source redshift

            Returns
            pzs: Prior probability density function (normalized)
        '''
        return self.pzs_unnormed(zs)/self.rate

    def pzs_unnormed(self, zs):
        ''' The redshift prior is defined with: 
            dNl/dt = int R_m(z)/(1+z_s) * tau(z_s) * dVc/dz * dz  = int R_L* p(z) dz

            Parameters
            zs: Source redshift

            Returns
            pzs: Prior probability density function (unnormalized)
        '''
        return self.Rm(zs)/(1+zs) * self.dVcdz_interp_z(zs)

    def draw_zs_samples(self, nsamples=1000, savesamples=True):
        ''' Draw samples from p(zs)

        Parameters
        nsamples: Number of samples to draw
        savesamples: If True, saves the samples in the class

        Returns
        zs_samples: Samples of source redshifts
        '''
        zs_samples = rejectionsample(self.zmin, self.zmax, self.pzs, size=nsamples)
        if savesamples == True:
            self.zs_samples = zs_samples
        return zs_samples

class LensStatistics(UnlensedStatistics):
    def __init__(self, Rm=RmOguri, cosmology=Planck18, zmin=0, zmax=30):
        ''' Initializes the class. Takes as input the merger-rate density and cosmology. 

        Parameters
        Rm: Merger-rate density function
        cosmology: Astropy cosmology object
        zmin, zmax: Minimum and maximum redshifts to consider
        '''
        super(LensStatistics, self).__init__( Rm, cosmology, zmin, zmax )
        # rate
        self.rate            = quad(lambda z: self.pzs_unnormed(z), zmin, zmax)[0] # Normalize p(z)
        self.n_images       = 2 # 2 images for the SIS model
        self.lens_model     = 'SIS'
        # Uninitalized:
        self.zs_samples     = np.nan
        self.zl_samples     = np.nan
        self.sigma_samples  = np.nan
        return

    def draw_samples(self, nsamples=1000, savesamples = True, seed=None):
        ''' Draw samples from the distribution of intrinsic parameters

        Parameters
        nsamples: Number of samples to draw
        savesamples: If True, saves the samples in the class
        seed: Random seed

        Returns
        samples: Dictionary of samples
        '''
        if seed is not None:
            np.random.seed(seed)
        # Draw source redshifts
        zs_samples = self.draw_zs_samples(nsamples=nsamples)
        # Draw vel. dispersion
        sigma_samples = self.draw_velocity_dispersion(nsamples=nsamples)
        # Draw lens redshift
        zl_samples = self.draw_zl_samples(zs_samples, nsamples=nsamples)
        # Transform to angular diameter distances (takes a while)
        DS_samples  = Planck18.angular_diameter_distance(zs_samples)
        DL_samples  = Planck18.angular_diameter_distance(zl_samples)
        DLS_samples = Planck18.angular_diameter_distance_z1z2(zl_samples,zs_samples)
        theta_E_samples = self.compute_theta_E_samples(sigma_samples, DLS_samples, DS_samples)
        # Compute weights for each sample:
        sample_weights = theta_E_samples**2/np.sum(theta_E_samples**2)
        # Draw source positions
        y_samples = self.draw_y_samples(nsamples = nsamples)
        # Sample fermat potentials:
        dtau_samples = self.compute_dtau_samples( y_samples, theta_E_samples)
        # Compute time delay
        Ddt_samples = self.compute_time_delay_distance( DL_samples, DS_samples, DLS_samples, zl_samples)
        dt_samples = Ddt_samples * dtau_samples
        # Compute magnifications
        mu_samples = self.compute_magnifications(y_samples)
        # Save samples
        samples    = dict(zs=zs_samples,
                          zl=zl_samples,
                          sigma=sigma_samples*u.km/u.second,
                          DS=DS_samples,
                          DL=DL_samples,
                          DLS=DLS_samples,
                          theta_E=theta_E_samples*u.radian,
                          weights=sample_weights,
                          y=y_samples,
                          dtau=dtau_samples,
                          Ddt=Ddt_samples*u.year,
                          dt=dt_samples*u.year,
                          mu=mu_samples)
        if savesamples == True:
            self.samples = samples # All samples here
            self.zs_samples=zs_samples
            self.zl_samples=zl_samples
            self.sigma_samples=sigma_samples
            self.DS_samples=DS_samples
            self.DL_samples=DL_samples
            self.DLS_samples=DLS_samples
            self.theta_E_samples=theta_E_samples
            self.samples_weights=sample_weights
            self.y_samples=y_samples
            self.dtau_samples=dtau_samples
            self.Ddt_samples=Ddt_samples
            self.dt_samples=dt_samples
            self.mu_samples=mu_samples
        return samples

    def psigma(self, sigma, alpha=2.32, beta=2.67):
        ''' prior probability distribution for the velocity dispersion sigma p(sigma|HL) see [https://arxiv.org/pdf/1807.07062.pdf]

        Parameters
        sigma: Velocity dispersion
        alpha, beta: Fitting parameters to the distribution

        Returns
        psigma: Prior probability density function p(sigma)

        '''
        pa = lambda a: a**(alpha-1)*np.exp(-a**beta) * beta/gamma(alpha/beta)
        a = sigma/self.sigma0
        return pa(a)/self.sigma0

    def tau(self, zs):
        ''' Strong lensing optical depth [https://arxiv.org/abs/1807.07062]

        Parameters
        zs: Source redshift

        Returns
        tau: Strong lensing optical depth
        '''
        return 4.17e-6 * (self.Dc_interp_z(zs))**3


    def pzs_unnormed(self, zs):
        ''' The redshift prior is defined with: 
            dNl/dt = int R_m(z)/(1+z_s) * tau(z_s) * dVc/dz * dz  = int R_L* p(z) dz

        Parameters
        zs: Source redshift

        Returns
        pzs: Unnormalized redshift prior
        '''
        return self.Rm(zs)/(1+zs) * self.tau(zs) * self.dVcdz_interp_z(zs)

    def draw_velocity_dispersion(self, nsamples=1000):
        ''' Draw samples from the distribution of velocity dispersions

        Parameters
        nsamples: Number of samples to draw

        Returns
        sigma_samples: Samples of velocity dispersions
        '''
        self.sigma = rejectionsample(0,800, self.psigma, size=nsamples)
        return self.sigma

    def draw_zl_samples(self, zs, nsamples=1000, savesamples=True):
        ''' Draw samples from p(zl|zs)

        Parameters
        zs: Source redshift
        nsamples: Number of samples to draw
        savesamples: If True, saves the samples in the class

        Returns
        zl_samples: Samples of lens redshifts
        '''
        # Now draw the lens redshift
        pr = lambda r: 30 * r**2*(1-r)**2
        r = rejectionsample(0,1,pr,size=nsamples)
        # Lens redshift
        Dlc = r*self.Dc_interp_z(zs) # Gpc
        zl_samples  = self.z_interp_Dc(Dlc) # zl
        if savesamples == True:
            self.zl_samples = zl_samples
        return zl_samples

    def draw_y_samples(self, nsamples=1000, savesamples=True):
        ''' Draw samples from the source position prior p(y) 

        Parameters
        nsamples: Number of samples to draw
        savesamples: If True, saves the samples in the class

        Returns
        y_samples: Samples of source positions
        '''
        # Inverse transform sample source position p(y) =2y (y<1)
        u = np.random.uniform(0,1,size=nsamples)
        y_samples = np.sqrt(u)
        if savesamples == True:
            self.y_samples = y_samples
        return y_samples

    def compute_theta_E_samples(self, sigma_samples, DLS_samples, DS_samples, savesamples=True):
        ''' Compute Einstein radius samples from the velocity dispersion samples

        Parameters
        sigma_samples: Samples of velocity dispersions
        DLS_samples: Samples of angular diameter distances between the lens and the source
        DS_samples: Samples of angular diameter distances between the source and the observer
        savesamples: If True, saves the samples in the class

        Returns
        theta_E_samples: Samples of Einstein radii
        '''
        # Now compute the Einstein radii
        c = 2.998e5 # km/s
        theta_E_samples = 4*np.pi*sigma_samples**2/c**2 * DLS_samples.value/DS_samples.value
        if savesamples == True:
            self.theta_E_samples = theta_E_samples
        return theta_E_samples

    def compute_dtau_samples(self, y, theta_E, savesamples=True):
        ''' Compute the time delay samples from the source position and Einstein radius samples

        Parameters
        y: Source position
        theta_E: Einstein radius
        savesamples: If True, saves the samples in the class

        Returns
        dtau_samples: Samples of time delays
        '''
        # Fermat potential:
        dtau_samples = 2 * y * theta_E**2
        if savesamples == True:
            self.dtau_samples = dtau_samples
        return dtau_samples

    def compute_time_delay_distance(self, DL, DS, DLS, zl, savesamples = True):
        ''' Compute the time delay distance from the angular diameter distances

        Parameters
        DL: Angular diameter distance between the observer and the lens
        DS: Angular diameter distance between the observer and the source
        DLS: Angular diameter distance between the lens and the source
        zl: Lens redshift
        savesamples: If True, saves the samples in the class

        Returns
        Ddt: Time delay distance samples
        '''
        # Compute time delay distance
        c_MpcPerYear = 3.064e-7
        Ddt_samples = DL.value*DS.value/(c_MpcPerYear*DLS.value)*(1+zl) # In units of years
        if savesamples == True:
            self.Ddt_samples = Ddt_samples
        return Ddt_samples

    def compute_magnifications(self, y, savesamples=True):
        ''' Compute the magnification samples from the source position samples

        Parameters
        y: Source position
        savesamples: If True, saves the samples in the class

        Returns
        mu_samples: Samples of magnifications
        '''
        # get the magnifications
        mu0 = 1+1/y
        mu1 = -1+1/y
        mu_samples = np.array([mu0, mu1])
        if savesamples == True:
            self.mu_samples = mu_samples
        return mu_samples

class LensStatisticsPEMDShear( LensStatistics ):
    def __init__(self, Rm=RmOguri, cosmology=Planck18, zmin=0, zmax=30):
        ''' Initializes the class. Takes as input the merger-rate density and cosmology. 

        Parameters
        Rm: Merger rate density
        cosmology: Cosmology
        zmin: Minimum redshift
        zmax: Maximum redshift
        '''
        super(LensStatisticsPEMDShear, self).__init__( Rm, cosmology, zmin, zmax )
        # qsamples
        self.q_samples        = np.nan
        # rate
        self.n_images         = 5 # 5 images for the PEMDShear model (max)
        self.lens_model       = 'PEMDShear'
        self. lens_model_list = ['EPL_NUMBA', 'SHEAR']
        return

    def draw_samples(self, nsamples=1000, savesamples = True, seed=None):
        ''' Draw samples from the lens statistics

        Parameters
        nsamples: Number of samples to draw
        savesamples: If True, saves the samples in the class
        seed: Seed for the random number generator

        Returns
        zl_samples: Samples of lens redshifts
        y_samples: Samples of source positions
        theta_E_samples: Samples of Einstein radii
        dtau_samples: Samples of time delays
        mu_samples: Samples of magnifications
        '''
        if seed is not None:
            np.random.seed(seed)
        # Draw source redshifts
        zs_samples = self.draw_zs_samples(nsamples=nsamples)
        # Draw vel. dispersion
        sigma_samples = self.draw_velocity_dispersion(nsamples=nsamples)
        # Draw lens redshift
        zl_samples = self.draw_zl_samples(zs_samples, nsamples=nsamples)
        # Transform to angular diameter distances (takes a while)
        DS_samples  = Planck18.angular_diameter_distance(zs_samples)
        DL_samples  = Planck18.angular_diameter_distance(zl_samples)
        DLS_samples = Planck18.angular_diameter_distance_z1z2(zl_samples,zs_samples)
        theta_E_samples = self.compute_theta_E_samples(sigma_samples, DLS_samples, DS_samples)*u.radian
        # Draw the axis ratios
        q_samples = self.draw_axis_ratio_samples( sigma_samples )
        phi_samples = np.random.uniform(0,2*np.pi,nsamples)
        e1_samples, e2_samples = phi_q2_ellipticity(phi_samples, q_samples)
        # Compute weights for each sample:
        sample_weights = theta_E_samples**2/np.sum(theta_E_samples**2)
        # power-law slope
        gamma_samples = self.draw_gamma_samples(nsamples)
        # Shears
        gamma1_samples, gamma2_samples = self.draw_shears(nsamples)
        # Draw image positions:
        x0_image_samples, x1_image_samples, mu_samples, td_samples, tau_samples, image_type_samples, y0_source_samples, y1_source_samples, n_image_samples = self.draw_pemd_shear_image_properties(theta_E_samples.to_value('arcsec'), e1_samples, e2_samples, gamma_samples, gamma1_samples, gamma2_samples, zl_samples, zs_samples)
        Ddt_samples = self.compute_time_delay_distance( DL_samples, DS_samples, DLS_samples, zl_samples)
        dt_samples = np.diff(td_samples, axis=0)
        # Save samples
        samples    = dict(zs=zs_samples,
                          zl=zl_samples,
                          sigma=sigma_samples*u.km/u.second,
                          DS=DS_samples,
                          DL=DL_samples,
                          DLS=DLS_samples,
                          theta_E=theta_E_samples,
                          weights=sample_weights,
                          y0_source=y0_source_samples,
                          y1_source=y1_source_samples,
                          x0_image=x0_image_samples,
                          x1_image=x1_image_samples,
                          tau=tau_samples,
                          image_type=image_type_samples,
                          Ddt=Ddt_samples*u.year,
                          dt=dt_samples*u.year,
                          td=td_samples*u.year,
                          mu=mu_samples,
                          n_images=n_image_samples)
        return samples

    def draw_gamma_samples(self, nsamples, savesamples=True):
        ''' Draw samples from the power-law slope

        Parameters
        nsamples: Number of samples to draw
        savesamples: If True, saves the samples in the class

        Returns
        gamma_samples: Samples of power-law slopes
        '''
        return norm.rvs(loc=2,scale=0.2, size=nsamples)

    def draw_shears( self, nsamples, savesamples=True):
        # Draw an external shear
        gamma_1_samples = norm.rvs(scale = 0.05,size=nsamples)
        gamma_2_samples = norm.rvs(scale = 0.05,size=nsamples)
        return gamma_1_samples, gamma_2_samples

    def draw_axis_ratio_samples(self, sigma_samples, savesamples=True):
        ''' Draws axis ratios from the Rayleight distribution (Eq. C16 of https://arxiv.org/pdf/2106.06303.pdf ) for every given velocity dispersion sample

        Parameters
        sigma_samples: Samples of velocity dispersions
        savesamples: If True, saves the samples in the class

        Returns
        q_samples: Samples of axis ratios
        '''
        nsamples = len(sigma_samples)
        a = sigma_samples/self.sigma0
        s = 0.38-0.09177*a
        c0 = 1./(1. - np.exp(-8./(25.*s**2))) # Normalizes the PDF
        u = np.random.uniform(0,1,nsamples)
        b_samples = np.sqrt(2.)*s*np.sqrt(np.log(c0/(c0 - u)))
        q_samples = 1-b_samples
        if savesamples == True:
            self.q_samples = q_samples
        return q_samples

    def compute_image_type(self, lensModel, x0_image, x1_image, kwargs_lens_list):
        ''' Computes the type of image (Einstein, caustic, critical, deflector) for every image

        Parameters
        lensModel: Lens Model
        x0_image: Image positions in the x0 direction
        x1_image: Image positions in the x1 direction
        kwargs_lens_list: List of lens model parameters

        Returns
        image_type: Type of image
        '''
        n_images_created = len(x0_image)
        image_types = np.zeros(n_images_created)
        for i in range(n_images_created):
            f_xx, f_xy, f_yx, f_yy = lensModel.hessian(x0_image[i], x1_image[i], kwargs_lens_list)
            determinant = (1-f_xx)*(1-f_yy) - f_xy*f_yx
            trace = 2 - f_xx - f_yy
            image_type = 0 
            if determinant < 0:
                image_type = 2
            elif trace > 0:
                image_type = 1
            elif trace < 0:
                image_type = 3
            image_types[i] = image_type
        return image_types

    def compute_pemd_shear_image_properties(self, theta_E, e1, e2, gamma, gamma1, gamma2, z_lens, z_source, y0_source, y1_source):
        ''' Computes image properties (magnifications, time delays, source positions) given the lens parameters.

        Parameters
        theta_E: Einstein radius
        e1: Ellipticity component in the x0 direction
        e2: Ellipticity component in the x1 direction
        gamma: Power-law slope
        gamma1: External shear component in the x0 direction
        gamma2: External shear component in the x1 direction
        z_lens: Lens redshift
        z_source: Source redshift
        y0_source: Source position in the x0 direction
        y1_source: Source position in the x1 direction

        Returns
        x0_image: Image positions in the x0 direction
        x1_image: Image positions in the x1 direction
        mu: Image magnifications
        td: Time delays
        tau: Time delays relative to the first image
        n_images_created: Number of images created by the lens
        '''
        # Create the simulated lens
        lens_model_list = self.lens_model_list
        kwargs_spemd = {'theta_E': theta_E, 'center_x': 0, 'center_y': 0, 'e1': e1, 'e2': e2, 'gamma': gamma}  # parameters of the deflector lens model
        kwargs_shear = {'gamma1': gamma1, 'gamma2': gamma2, "ra_0": 0, "dec_0": 0}  # shear values to the source plane
        kwargs_lens_list = [kwargs_spemd, kwargs_shear]
        lensModel = LensModel(lens_model_list, z_lens=z_lens, z_source=z_source)
        lensEquationSolver = LensEquationSolver(lensModel)
        # Save the lens model and solver
        self.lensModel = lensModel
        self.lensEquationSolver = lensEquationSolver
        self.kwargs_lens_list = kwargs_lens_list
        # Create the simulated GW image properties:
        #x0_image, x1_image = lensEquationSolver.image_position_from_source(kwargs_lens=kwargs_lens_list, sourcePos_x=y0_source, sourcePos_y=y1_source, min_distance=theta_E/50, search_window=theta_E*4, precision_limit=10**(-7), num_iter_max=1000)#, solver='analytical')
        x0_image, x1_image = lensEquationSolver.image_position_from_source(kwargs_lens=kwargs_lens_list, sourcePos_x=y0_source, sourcePos_y=y1_source, solver='analytical')
        mu = lensModel.magnification(x0_image, x1_image, kwargs_lens_list)
        td  = lensModel.arrival_time(x0_image, x1_image, kwargs_lens_list)*24*60*60 # Seconds
        tau = lensModel.fermat_potential(x0_image, x1_image, kwargs_lens_list)
        n_images_created = len(mu)
        # Compute the morse phase
        image_type = self.compute_image_type(lensModel, x0_image, x1_image, kwargs_lens_list)
        if n_images_created>self.n_images:
            raise ValueError(n_images_created,"is bigger than the max number of images:",self.n_images)
        # Ensure that the arrays that are returned have a fixed shape
        missing_images = np.ones(self.n_images-n_images_created)*np.nan
        mu = np.append(mu, missing_images)
        td = np.append(td, missing_images)
        tau = np.append(td, missing_images)
        x0_image = np.append(x0_image, missing_images)
        x1_image = np.append(x1_image, missing_images)
        image_type = np.append(image_type, missing_images)
        return x0_image, x1_image, mu, td, tau, image_type, n_images_created

    def draw_pemd_shear_image_properties(self, theta_E_samples, e1_samples, e2_samples, gamma_samples, gamma1_samples, gamma2_samples, z_lens_samples, z_source_samples):
        ''' Draws image properties (magnifications, time delays, source positions) given the lens samples. Guarantees that every drawn source position produces at least 2 images. 

            ARGS:
            -----
            theta_E_samples: array, samples for the einstein radius [arcsecond units]
            e1_samples: array, samples for the 1st component of ellipticity
            e2_samples: array, samples for the 2nd component of ellipticity
            gamma_saples: array, samples for the power-law slope of the lens profile
            gamma1_samples: array, samples for the 1st shear component
            gamma2_samples: array, samples for the 2nd shear component
            z_lens_samples: array, samples for the lens redshifts
            z_source_samples: array, samples for the source redshift

            returns: x0_image_samples, x1_image_samples, mu_samples, td_samples, y0_source_samples, y1_source_samples, n_image_samples
            x0_image_samples: (n_images,N) array of image x-position on the image plane (nan if no images; sorted in time)
            x1_image_samples: (n_images,N) array of image y-position on the image plane (nan if no images; sorted in time)
            mu_samples: is (n_images,N) array of magnifications for each image (nan if no images; sorted in time)
            td_samples: (n_images,N) array, time delays for each image (nan if no images; sorted in time)
            tau_samples: (n_images,N) array, fermat potentials for each image (nan if no images; sorted in time)
            image_type_samples: (n_images,N) array, image types for each image (nan if no images; sorted in time)
            y0_source_samples: (n_images,N) array of source positions (0 component)
            y1_source_samples: (n_images,N) array of source positions (1 component)
            n_image_samples: int, number of images created for each sample
        '''
        nsamples = len(theta_E_samples)
        i = 0
        x0_image_samples, x1_image_samples, mu_samples, td_samples, tau_samples, image_type_samples, y0_source_samples, y1_source_samples, n_image_samples = [], [], [], [], [], [], [], [], []
        while(i < nsamples): # Draw images until we get one set of images for each sample
            # Get the lens properties
            theta_E, e1, e2, gamma, gamma1, gamma2, z_lens, z_source = theta_E_samples[i], e1_samples[i], e2_samples[i], gamma_samples[i], gamma1_samples[i], gamma2_samples[i], z_lens_samples[i], z_source_samples[i]
            #print(i, theta_E, e1, e2, gamma, gamma1, gamma2, z_lens, z_source)
            # Draw the source position (within 1.5 einstein radii):
            y_magnitude_max = 1.5*theta_E; c0 = 2./y_magnitude_max**2; u = np.random.uniform(0,1); y_magnitude = (np.sqrt(2)*np.sqrt(u))/np.sqrt(c0)
            y_phi = np.random.uniform(0,2*np.pi)
            y0_source, y1_source = y_magnitude*np.cos(y_phi), y_magnitude*np.sin(y_phi) # Polar to cartesian coordinates
            # Compute the image properties
            x0_image, x1_image, mu, td, tau, image_type, n_images_created = self.compute_pemd_shear_image_properties(theta_E, e1, e2, gamma, gamma1, gamma2, z_lens, z_source, y0_source, y1_source)
            # if strongly lensed, store samples, if not, continue loop
            sample_is_strongly_lensed = (n_images_created > 1)
            if sample_is_strongly_lensed == True:
                x0_image_samples.append(x0_image)
                x1_image_samples.append(x1_image)
                mu_samples.append(mu)
                td_samples.append(td)
                tau_samples.append(tau)
                image_type_samples.append(image_type)
                y0_source_samples.append(y0_source)
                y1_source_samples.append(y1_source)
                n_image_samples.append(n_images_created)
                i = i + 1
                if i%10000 == 0:
                    print("Iteration",i,"out of",nsamples)
        # Return numpy array in (n_images, N) format:
        x0_image_samples, x1_image_samples, mu_samples, td_samples, tau_samples, image_type_samples= np.transpose(x0_image_samples), np.transpose(x1_image_samples), np.transpose(mu_samples), np.transpose(td_samples), np.transpose(tau_samples), np.transpose(image_type_samples)
        y0_source_samples, y1_source_samples, n_image_samples  = np.array(y0_source_samples), np.array(y1_source_samples), np.array(n_image_samples)
        return x0_image_samples, x1_image_samples, mu_samples, td_samples, tau_samples, image_type_samples, y0_source_samples, y1_source_samples, n_image_samples

class UnlensedStatisticsBNS(UnlensedStatistics):
    def draw_samples(self, nsamples=1000, savesamples = True, seed=None):
        '''
        Draw samples from the prior distribution of the unlensed BNS system
        '''
        if seed is not None:
            np.random.seed(seed)
        # Draw source redshifts
        zs_samples = self.draw_zs_samples(nsamples=nsamples)
        # Transform to angular diameter distances (takes a while)
        DS_samples  = Planck18.angular_diameter_distance(zs_samples)
        # Compute weights for each sample:
        sample_weights = self.pzs(zs_samples)/self.pzs_uniform(zs_samples)
        # Save samples
        samples    = dict(zs=zs_samples,
                          DS=DS_samples,
                          weights=sample_weights)
        if savesamples == True:
            self.samples = samples # All samples here
            self.zs_samples=zs_samples
            self.samples_weights=sample_weights
        return samples

    def pzs_uniform(self, zs):
        ''' Uniform redshift prior
        '''
        c0 = 1./np.log((0.01 + self.zmax)/(0.01 + self.zmin))
        return c0*(0.01+zs)**(-1)

    def draw_zs_samples(self, nsamples=1000, savesamples=True):
        ''' Draw samples from p(zs)

        '''
        zs_samples = rejectionsample(self.zmin, self.zmax, self.pzs_uniform, size=nsamples)
        if savesamples == True:
            self.zs_samples = zs_samples
        return zs_samples

class LensStatisticsBNS(LensStatistics):
    def draw_samples(self, nsamples=1000, savesamples = True, seed=None):
        if seed is not None:
            np.random.seed(seed)
        # Draw source redshifts
        zs_samples = self.draw_zs_samples(nsamples=nsamples)
        # Draw vel. dispersion
        sigma_samples = self.draw_velocity_dispersion(nsamples=nsamples)
        # Draw lens redshift
        zl_samples = self.draw_zl_samples(zs_samples, nsamples=nsamples)
        # Transform to angular diameter distances (takes a while)
        DS_samples  = Planck18.angular_diameter_distance(zs_samples)
        DL_samples  = Planck18.angular_diameter_distance(zl_samples)
        DLS_samples = Planck18.angular_diameter_distance_z1z2(zl_samples,zs_samples)
        theta_E_samples = self.compute_theta_E_samples(sigma_samples, DLS_samples, DS_samples)
        # Compute weights for each sample:
        sample_weights = theta_E_samples**2/np.sum(theta_E_samples**2) * self.pzs(zs_samples)/self.pzs_uniform(zs_samples)
        # Draw source positions
        y_samples = self.draw_y_samples(nsamples = nsamples)
        # Sample fermat potentials:
        dtau_samples = self.compute_dtau_samples( y_samples, theta_E_samples)
        # Compute time delay
        Ddt_samples = self.compute_time_delay_distance( DL_samples, DS_samples, DLS_samples, zl_samples)
        dt_samples = Ddt_samples * dtau_samples
        # Compute magnifications
        mu_samples = self.compute_magnifications(y_samples)
        # Save samples
        samples    = dict(zs=zs_samples,
                          zl=zl_samples,
                          sigma=sigma_samples*u.km/u.second,
                          DS=DS_samples,
                          DL=DL_samples,
                          DLS=DLS_samples,
                          theta_E=theta_E_samples*u.radian,
                          weights=sample_weights,
                          y=y_samples,
                          dtau=dtau_samples,
                          Ddt=Ddt_samples*u.year,
                          dt=dt_samples*u.year,
                          mu=mu_samples)
        if savesamples == True:
            self.samples = samples # All samples here
            self.zs_samples=zs_samples
            self.zl_samples=zl_samples
            self.sigma_samples=sigma_samples
            self.DS_samples=DS_samples
            self.DL_samples=DL_samples
            self.DLS_samples=DLS_samples
            self.theta_E_samples=theta_E_samples
            self.samples_weights=sample_weights
            self.y_samples=y_samples
            self.dtau_samples=dtau_samples
            self.Ddt_samples=Ddt_samples
            self.dt_samples=dt_samples
            self.mu_samples=mu_samples
        return samples
    def pzs_uniform(self, zs):
        ''' Uniform redshift prior
        '''
        c0 = 1./np.log((0.01 + self.zmax)/(0.01 + self.zmin))
        return c0*(0.01+zs)**(-1)
    def draw_zs_samples(self, nsamples=1000, savesamples=True):
        ''' Draw samples from p(zs)

        '''
        zs_samples = rejectionsample(self.zmin, self.zmax, self.pzs_uniform, size=nsamples)
        if savesamples == True:
            self.zs_samples = zs_samples
        return zs_samples

# Helper functions
###########
def make_cosmology_interpolators(cosmology, zmin=0, zmax=30):
    # Make an interpolator for the comoving distance
    z              = np.linspace(zmin,zmax,int(1e5))
    Dc             = cosmology.comoving_distance(z).value/1e3 # Mpc to Gpc
    Dc_interp_z    = interp1d(z,Dc)
    z_interp_Dc    = interp1d(Dc,z)
    dVcdz          = 4*np.pi*cosmology.differential_comoving_volume(z).value/1e9 # Mpc3 to Gpc3
    dVcdz_interp_z = interp1d(z,dVcdz)
    z_interp_dVcdz = interp1d(dVcdz,z)
    return z_interp_Dc, z_interp_dVcdz, Dc_interp_z, dVcdz_interp_z
def rejectionsample(xmin, xmax, pa, b=[], size=1000):
    # rejection sampling
    fmax = np.max(pa(np.random.uniform(xmin,xmax,size=size*2)))
    a    = np.random.uniform(xmin,xmax,size=size*2)
    f    = np.random.uniform(0,fmax,size=size*2)
    pa_array = pa(a)
    # Choose the non-rejected ones:
    a    = a[f<pa_array]
    # Append previous results
    a    = np.append(a,b)
    if len(a) > size:
        return a[:size]
    else:
        return rejectionsample(xmin, xmax, pa, b=a,size=size) # iterate
###########
